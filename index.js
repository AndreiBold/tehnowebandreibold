const FIRST_NAME = "Andrei";
const LAST_NAME = "Bold";
const GRUPA = "1085";

// class EmptyObject {
//     aboutCounter = 0;
//     homeCounter = 0;
// }

/**
 * Make the implementation here
 */

// function initCaching() {
//   var cache = {
//     // emptyObject: {
//     //   aboutCounter: 0,
//     //   homeCounter: 0
//     // },

//     pageAccessCounter: function(partOfTheSite) {
//       if (partOfTheSite == "about") {
//         emptyObject.aboutCounter++;
//       }

//       if (
//         partOfTheSite == "home" ||
//         partOfTheSite == "contact" ||
//         partOfTheSite == null
//       ) {
//         emptyObject.homeCounter++;
//       }
//     },

//     getCache: function() {
//       return this.emptyObject;
//     }
//   };

//   return cache;
// }

function initCaching() {
  var myObj = {
    pageAccessCounter(partOfTheSite) {
      if (partOfTheSite == undefined || partOfTheSite == null) {
        myObj.cache['home'] += 1;
      } else {
      if (myObj.cache[partOfTheSite.toLowerCase()] == undefined)
          myObj.cache[partOfTheSite.toLowerCase()] = 1;
      else 
          myObj.cache[partOfTheSite.toLowerCase()] += 1;
        }
    },
    getCache() {
      return myObj.cache;
    }
  };
  myObj.cache = {};
  myObj.cache.home = 0;

  return myObj;
}

module.exports = {
  FIRST_NAME,
  LAST_NAME,
  GRUPA,
  initCaching
};
